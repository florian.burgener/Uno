/*jshint esversion: 6 */

const RoomManager = require('./RoomManager.js');
const xss = require('xss-filters');

let app = require('express')();
let server = require('http').Server(app);
let io = require('socket.io')(server);
let roomManager = new RoomManager(io);

server.listen(4040);

io.on('connection', (socket) => {
    socket.on('setUsername', (username) => {
        socket.username = xss.inHTMLData(username);
    });

    socket.on('getUsername', () => {
        socket.emit('username', socket.username);
    });

    roomManager.initSocket(socket);
});
