/*jshint esversion: 6 */

const Uno = require('./Uno.js');

module.exports = class Game {
    constructor(io, room) {
        this.io = io;
        this.room = room;
        this.setSockets();
        this.uno = new Uno(this.sockets.length);

        for (let i in this.sockets) {
            if (this.sockets.hasOwnProperty(i)) {
                this.initSocket(this.sockets[i], i);
            }
        }

        for (let i in this.sockets) {
            if (this.sockets.hasOwnProperty(i)) {
                this.sockets[i].emit('startGame', this.getState(this.sockets[i]));
            }
        }
    }

    initSocket(socket, playerIndex) {
        let self = this;
        socket.playerIndex = playerIndex;

        socket.on('play', (cardIndex, cardColor) => {
            self.uno.playerPlay(socket.playerIndex, cardIndex, cardColor);
            self.update();
        });

        socket.on('draw', () => {
            self.uno.playerDraw(socket.playerIndex);
            self.update();
        });

        socket.on('disconnect', () => {
            let playerIndex = socket.playerIndex;
            this.sockets.splice(playerIndex, 1);
            this.uno.deletePlayer(playerIndex);

            for (let i = 0; i < this.sockets.length; i++) {
                this.sockets[i].playerIndex = i;
            }

            self.update();
        });
    }

    setSockets() {
        let roomSockets = this.io.sockets.adapter.rooms[this.room].sockets;
        let sockets = [];

        for (let socket in roomSockets) {
            if (roomSockets.hasOwnProperty(socket)) {
                sockets.push(this.io.sockets.connected[socket]);
            }
        }

        this.sockets = sockets;
    }

    update() {
        for (let i in this.sockets) {
            if (this.sockets.hasOwnProperty(i)) {
                this.sockets[i].emit('update', this.getState(this.sockets[i]));
            }
        }
    }

    getState(socket) {
        let sockets = this.sockets.slice();

        sockets.sort(function (a, b) {
            if (a.index < b.index) return -1;
            if (a.index > b.index) return 1;
            return 0;
        });

        for (let i = 0; i < socket.playerIndex; i++) {
            let tmp = sockets.shift();
            sockets.push(tmp);
        }

        let players = [];

        for (let i in sockets) {
            if (sockets.hasOwnProperty(i)) {
                let player = {
                    username: sockets[i].username + (this.uno.isCurrentPlayer(sockets[i].playerIndex) ? ' <---' : ''),
                };

                if (i == 0) {
                    player.cards = this.uno.players[sockets[i].playerIndex].cards.map((x, i) => {
                        return {
                            name: x.name,
                            cardIndex: i,
                        };
                    });
                } else {
                    player.cards = Array.apply(null, new Array(this.uno.players[sockets[i].playerIndex].cards.length)).map(x => {
                        return {
                            name: 'Back',
                        };
                    });
                }

                players.push(player);
            }
        }

        let lastPlayedCard = this.uno.getLastPlayedCard();

        return {
            lastPlayedCard: {
                name: lastPlayedCard.name,
                color: lastPlayedCard.color,
            },
            cardCountByDraw: this.uno.cardCountByDraw,
            players: players,
        };
    }
};
