/*jshint esversion: 6 */

const xss = require('xss-filters');
const Game = require('./Game.js');

module.exports = class Room {
    constructor(io, name) {
        this.io = io;
        this.name = name;
        this.game = null;
    }

    room() {
        return this.io.sockets.adapter.rooms[this.name];
    }

    count() {
        return this.room().length;
    }

    sockets() {
        return this.room().sockets;
    }

    emitPlayers() {
        let sockets = this.sockets();
        let players = [];

        for (let socket in sockets) {
            if (sockets.hasOwnProperty(socket)) {
                players.push(this.io.sockets.connected[socket].username);
            }
        }

        players.sort();

        this.io.to(this.name).emit('players', players);
    }

    setNewAdmin() {
        if (this.game != null) return;

        let sockets = this.sockets();

        for (let s in sockets) {
            if (sockets.hasOwnProperty(s)) {
                let socket = this.io.sockets.connected[s];
                socket.emit('admin');
                socket.isAdmin = true;
                break;
            }
        }
    }

    startGame(socket) {
        if ((socket.isAdmin) && (this.count() >= 2)) {
            this.game = new Game(this.io, socket.room);
        }
    }
};
