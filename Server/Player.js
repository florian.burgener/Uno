/*jshint esversion: 6 */

module.exports = class Player {
    constructor(cardCount) {
        this.cards = new Array(cardCount);
    }
};
