/*jshint esversion: 6 */

const xss = require('xss-filters');
const Room = require('./Room.js');

module.exports = class RoomManager {
    constructor(io) {
        this.io = io;
        this.rooms = {};
    }

    initSocket(socket) {
        socket.room = '';
        socket.isAdmin = false;

        socket.on('getRooms', () => {
            this.emitRooms();
        });

        socket.on('createRoom', (roomName) => {
            roomName = xss.inHTMLData(roomName);

            if (this.rooms[roomName] != null) {
                console.log(socket.username + ' try to create a second time this room : ' + roomName);
                return;
            }

            socket.isAdmin = true;
            this.rooms[roomName] = new Room(this.io, roomName);
        });

        socket.on('joinRoom', (roomName) => {
            roomName = xss.inHTMLData(roomName);
            this.joinRoom(socket, roomName);
        });

        socket.on('leaveRoom', () => {
            this.leaveRoom(socket);
        });

        socket.on('disconnecting', () => {
            this.leaveRoom(socket);
        });

        socket.on('startGame', () => {
            if (socket.isAdmin) {
                this.rooms[socket.room].startGame(socket);
                this.emitRooms();
            }        
        });
    }

    getRooms() {
        let roomInfos = [];

        for (let room in this.rooms) {
            if (this.rooms.hasOwnProperty(room)) {
                if ((this.rooms[room].game != null) || (this.rooms[room].count() >= 4)) continue;

                roomInfos.push({
                    name: this.rooms[room].name,
                    count: this.rooms[room].count(),
                });
            }
        }

        roomInfos.sort();

        return roomInfos;
    }

    emitRooms() {
        let sockets = this.io.sockets.connected;
        let roomInfos = this.getRooms();

        for (let socket in sockets) {
            if (sockets.hasOwnProperty(socket)) {
                if (sockets[socket].room === '') {
                    sockets[socket].emit('rooms', roomInfos);
                }
            }
        }
    }

    joinRoom(socket, roomName) {
        let that = this;

        socket.room = roomName;
        socket.join(roomName, function () {
            that.rooms[roomName].emitPlayers();
            that.emitRooms();
        });
    }

    leaveRoom(socket) {
        if (socket.room === '') return;

        let count = this.rooms[socket.room].count();
        socket.leave(socket.room);

        if (count > 1) {
            this.rooms[socket.room].emitPlayers();

            if (socket.isAdmin) {
                socket.isAdmin = false;

                this.rooms[socket.room].setNewAdmin();
            }
        }
        else {
            delete this.rooms[socket.room];
        }

        socket.room = '';
        this.emitRooms();
    }
};
