/*jshint esversion: 6 */

const Card = require('./Card.js');
const Player = require('./Player.js');

const CARD_COUNT_BY_PLAYER = 7;

Array.prototype.transfer = (other) => {
    for (let i = 0; i < this.length - 1; i++) {
        other.push(this[i]);
        this.splice(0, 1);
    }
};

Number.prototype.mod = function(n) {
    return ((this % n) + n) % n;
};

module.exports = class Uno {
    constructor(playerCount) {
        this.cards1 = [];
        this.cards2 = [];
        this.players = new Array(playerCount);
        this.cardCountByDraw = 1;
        this.currentPlayerIndex = Math.floor(Math.random() * (playerCount - 1));
        this.sense = 1;

        this.createCards();
        this.shuffle(this.cards1);
        this.distribute();

        // Flip the first card.
        this.cards2.push(this.cards1[0]);
        this.cards1.splice(0, 1);
    }

    createCards() {
        this.createCardsByColor('R');
        this.createCardsByColor('Y');
        this.createCardsByColor('G');
        this.createCardsByColor('B');

        for (let i = 0; i < 4; i++) {
            this.cards1.push(new Card('SC', this.getRandomColor(), (card, lastPlayedCard) => {
                return this.cardCountByDraw <= 1;
            }));
            this.cards1.push(new Card('SM', this.getRandomColor(), (card, lastPlayedCard) => true, (uno) => {
                this.addCardCountByDraw(4);
            }));
        }
    }

    createCardsByColor(color) {
        let baseComparing = (card, lastPlayedCard) => {
            if (this.cardCountByDraw >= 2) {
                return false;
            }

            if ((lastPlayedCard.name == 'SC') || (lastPlayedCard.name == 'SM')) {
                // Comparing the special card color with the color of the card.
                return lastPlayedCard.color == card.name[0];
            }

            // Comparing the last played card with the color of the card or the type.
            return (lastPlayedCard.name[0] == card.name[0]) || (lastPlayedCard.name[1] == card.name[1]);
        };

        for (let i = 0; i <= 9; i++) {
            for (let j = 0; j < 2; j++) {
                this.cards1.push(
                    new Card(`${color}${i}`, null, baseComparing)
                );

                if (i == 0) break;
            }
        }

        for (let i = 0; i < 2; i++) {
            this.cards1.push(new Card(`${color}P`, null, baseComparing, (uno) => {
                // Skip the next player.
                this.pass();
            }));

            this.cards1.push(new Card(`${color}R`, null, baseComparing, (uno) => {
                this.sense *= -1;
            }));

            this.cards1.push(new Card(`${color}M`, null, (card, lastPlayedCard) => {
                if ((lastPlayedCard.name == 'SC') || (lastPlayedCard.name == 'SM')) {
                    // Comparing the special card color with the color of the card.
                    return lastPlayedCard.color == card.name[0];
                }

                // Comparing the last played card with the color of the card or the type.
                return (lastPlayedCard.name[0] == card.name[0]) || (lastPlayedCard.name[1] == card.name[1]);
            }, (uno) => {
                this.addCardCountByDraw(2);
            }));
        }
    }

    distribute() {
        for (let i = 0; i < this.players.length; i++) {
            this.players[i] = new Player(CARD_COUNT_BY_PLAYER);

            for (let j = 0; j < this.players[i].cards.length; j++) {
                this.players[i].cards[j] = this.cards1.shift();
            }
        }
    }

    getLastPlayedCard() {
        return this.cards2[this.cards2.length - 1];
    }

    getRandomColor() {
        let colors = ['R', 'Y', 'G', 'B'];

        return colors[Math.floor(Math.random() * colors.length)];
    }

    addCardCountByDraw(val) {
        if (this.cardCountByDraw == 1) {
            this.cardCountByDraw = 0;
        }

        this.cardCountByDraw += val;
    }

    pass() {
        do {
            this.currentPlayerIndex = (this.currentPlayerIndex + this.sense).mod(this.players.length);
        } while (this.players.length >= 2 && this.players[this.currentPlayerIndex].cards.length <= 0);
    }

    playerCanPlay(playerIndex) {
        return this.currentPlayerIndex == playerIndex;
    }

    playerCanDraw(playerIndex) {
        for (let i in this.players[playerIndex].cards) {
            if (this.players[playerIndex].cards.hasOwnProperty(i)) {
                if (this.players[playerIndex].cards[i].canPlay(this.players[playerIndex].cards[i], this.getLastPlayedCard())) {
                    return false;
                }
            }
        }

        return true;
    }

    transferCards2ToCards1() {
        this.cards2.transfer(this.cards1);
        this.shuffle(this.cards1);
    }

    playerPlay(playerIndex, cardIndex, cardColor) {
        if (!this.playerCanPlay(playerIndex)) {
            // Not his turn.
            return;
        }

        if ((cardIndex >= this.players[playerIndex].cards.length) || (cardIndex < 0)) {
            // Card doesn't exist.
            return;
        }

        // Set the color of the card.
        this.players[playerIndex].cards[cardIndex].color = cardColor;

        if (this.players[playerIndex].cards[cardIndex].play(this)) {
            // The card is played.

            this.cards2.push(this.players[playerIndex].cards[cardIndex]);
            this.players[playerIndex].cards.splice(cardIndex, 1);

            this.pass();
        }
    }

    playerDraw(playerIndex) {
        if (!this.playerCanPlay(playerIndex)) {
            // Not his turn.
            return;
        }

        if (!this.playerCanDraw(playerIndex)) {
            // Can play so can't draw card.
            return;
        }

        for (let i = 0; i < this.cardCountByDraw; i++) {
            if (this.cards1.length <= 0) {
                this.transferCards2ToCards1();
            }

            this.players[playerIndex].cards.push(this.cards1[0]);
            this.cards1.splice(0, 1);
        }

        this.cardCountByDraw = 1;

        if (this.playerCanDraw(playerIndex)) {
            // Can't play after draw card.
            this.pass();
        }
    }

    isCurrentPlayer(playerIndex) {
        return playerIndex == this.currentPlayerIndex;
    }

    shuffle(a) {
        var j, x, i;
        for (i = a.length - 1; i > 0; i--) {
            j = Math.floor(Math.random() * (i + 1));
            x = a[i];
            a[i] = a[j];
            a[j] = x;
        }
    }

    deletePlayer(playerIndex) {
        this.players.splice(playerIndex, 1);

        if (this.currentPlayerIndex == this.players.length) {
            this.currentPlayerIndex = 0;
        }
        
        if (this.players.length <= 0) 
            return;

        if (this.players[this.currentPlayerIndex].cards.length <= 0) {
            // When the new curent player has no cards.
            this.pass();
        }
    }
};
