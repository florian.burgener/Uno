/*jshint esversion: 6 */

module.exports = class Card {
    constructor(name, color, canPlay, onPlay = null) {
        this.name = name;
        this.color = color;
        this.canPlay = canPlay;
        this.onPlay = onPlay;
    }

    play(uno) {
        if (this.canPlay(this, uno.getLastPlayedCard())) {
            if (this.onPlay !== null)
                this.onPlay(uno);

            return true;
        }

        return false;
    }
};
