/*jshint esversion: 6 */

let card = null;

socket.on('startGame', function (state) {
    changeContent(
        `<h3 class="leftUsername" id="leftUsername"></h3>
        <div class="tmp-hand" id="left">
        </div>

        <h3 class="topUsername" id="topUsername"></h3>
        <div class="tmp-hand" id="top">
        </div>

        <h3 class="rightUsername" id="rightUsername"></h3>
        <div class="tmp-hand" id="right">
        </div>

        <h3 class="bottomUsername" id="bottomUsername"></h3>
        <div class="tmp-hand" id="bottom">
        </div>

        <div>
        <div class="pick" id="pick">
        <img src="img/cards/Back.png" alt="Back" />
        </div>
        <div class="pick cardCountByDraw" id="cardCountByDraw">1</div>

        <div class="played" id="played">
        </div>
        <div class="played playedCardColor" id="playedCardColor"></div>
        </div>

        <div class="modal" tabindex="-1" role="dialog" id="colorChoose">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title">Choisissez la couleur que vous voulez!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        <button type="button" class="btn btn-secondary btn-color" data-dismiss="modal" id="redBtn" value="R">Rouge</button>
        <button type="button" class="btn btn-secondary btn-color" data-dismiss="modal" id="yellowBtn" value="Y">Jaune</button>
        <button type="button" class="btn btn-secondary btn-color" data-dismiss="modal" id="greenBtn" value="G">Vert</button>
        <button type="button" class="btn btn-secondary btn-color" data-dismiss="modal" id="blueBtn" value="B">Bleu</button>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" id="cancelColorChoice" data-dismiss="modal">Annuler</button>
        </div>
        </div>
        </div>
        </div>`,
        function () {
            update(state);
            $('#played').droppable({
                tolerance: 'pointer',
                drop: function (event, ui) {
                    let cardCode = $(ui.draggable).children().attr('alt');
                    let cardIndex = $(ui.draggable).children().attr('value');

                    if ((cardCode == 'SC') || (cardCode == 'SM')) {
                        card = cardIndex;
                        $('#colorChoose').modal('show');
                    } else {
                        socket.emit('play', cardIndex, null);
                    }
                }
            });
        }
    );
});

socket.on('update', function (state) {
    update(state);
});

$('body').on('click', '#pick', function () {
    socket.emit('draw');
});

$('body').on('click', '.btn-color', function () {
    socket.emit('play', card, $(this).val());
});

$('body').on('click', '#cancelColorChoice', function () {
    $('.tmp-card-playable').removeAttr('style');
    $('.tmp-card-playable').css('position', 'relative');
});

$('body').on('mousedown', '.tmp-card-playable', function () {
    $('.tmp-card-playable').css('z-index','1');
    $(this).css('z-index','1000');
});

function update(state) {
    updateHands(state);
    updateHeaps(state);
}

function updateHands(state) {
    let ids = [
        '#bottom',
        '#left',
        '#top',
        '#right',
    ];

    for (let i = 0; i < ids.length; i++) {
        $(ids[i] + 'Username').empty();
        $(ids[i]).empty();
    }

    for (let i in state.players) {
        $(ids[i] + 'Username').text(state.players[i].username);

        updateCards(ids[i], state.players[i].cards);
    }

    $('.tmp-card-playable').draggable({
        appendTo: '.container',
        revert : function(event, ui) {
            $(this).data("uiDraggable").originalPosition = {
                top : 0,
                left : 0
            };

            return !event;
        }
    });
}

function updateCards(id, cards) {
    let isMainPlayer = id == '#bottom';

    for (let i in cards) {
        $(id).append(
            `
            <div class="tmp-card-container ${isMainPlayer ? 'tmp-card-playable' : ''}">
            <img class="tmp-card" ${isMainPlayer ? 'id=card' : ''} alt="${cards[i].name}" value="${cards[i].cardIndex}" src="img/cards/${cards[i].name}.png" />
            </div>
            `
        );
    }
}

function updateHeaps(state) {
    $('#played').html(`<img src="img/cards/${state.lastPlayedCard.name}.png" alt="${state.lastPlayedCard.name}" />`);
    $('#cardCountByDraw').text(state.cardCountByDraw);

    if (state.lastPlayedCard.color != null) {
        $('#playedCardColor').text(convertColorAcronymToFrench(state.lastPlayedCard.color));
    } else {
        $('#playedCardColor').text(convertColorAcronymToFrench(state.lastPlayedCard.name[0]));
    }
}

function convertColorAcronymToFrench(acronym) {
    switch (acronym) {
        case 'R':
            return 'Rouge';
        case 'Y':
            return 'Jaune';
        case 'G':
            return 'Vert';
        case 'B':
            return 'Bleu';
    }
}
