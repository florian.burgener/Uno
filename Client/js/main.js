/*jshint esversion: 6 */

let socket = io('http://localhost:4040', {
    'reconnectionAttempts': 3,
});

socket.on('connect', function () {
    // Username page.
    changeContent(
        `
        <div class="top-username">
        <h1>Pseudo</h1>

        <div class="row">
        <div class="offset-md-4 col-md-4">
        <div class="input-group">
        <input type="text" class="form-control" placeholder="Entrer votre pseudo" id="username" autofocus />
        <div class="input-group-append">
        <button class="btn btn-primary" type="button" id="setUsername">Go</button>
        </div>
        </div>
        </div>
        </div>
        </div>
        `
    );
});

socket.on('connect_error', function (e) {
    changeContent('<h1>Connexion au serveur impossible</h1>');
});

socket.on('disconnect', function () {
    changeContent('<h1>Connexion avec le serveur perdue</h1>');
});

socket.on('rooms', function (rooms) {
    showRooms(rooms);
});

socket.on('players', function (players) {
    $('#players').empty();

    for (let i in players) {
        $('#players').append(`<li>${players[i]}</li>`);
    }
});

socket.on('admin', function () {
    addAdminButton();
});

$('body').on('click', '#setUsername', function () {
    setUsername();
});

$('body').on('keypress', '#username', function (e) {
    if (e.which == 13) {
        setUsername();
    }
});

$('body').on('click', '#startGame', function () {
    socket.emit('startGame');
});

$('body').on('click', '#createRoom', function () {
    createRoom();
});

$('body').on('keypress', '#roomName', function (e) {
    if (e.which == 13) {
        createRoom();
    }
});

$('body').on('click', '#joinRoom', function () {
    joinRoom($(this).text());
});

$('body').on('click', '#leaveRoom', function () {
    leaveRoom();
});

function changeContent(html, callback = null) {
    $('#content').fadeOut(500);

    setTimeout(() => {
        $('#content').fadeIn();
        $('#content').html(html);

        if (callback !== null) {
            callback();
        }
    }, 500);
}

function setUsername() {
    let username = $('#username').val().trim();

    // Username must be not empty.
    if (!username) return;

    socket.emit('setUsername', username);

    goRooms();
}

function goRooms() {
    changeContent(
        `
        <div class="text-center">
        <img class="img-fluid mb-2" src="img/logoUno.png" style="width: 200px;" />
        </div>

        <h1>Salons actifs</h1>

        <div id="rooms">

        </div>

        <h2>Créer un salon</h2>

        <div class="row">
        <div class="offset-md-4 col-md-4">
        <div class="input-group">
        <input type="text" class="form-control" placeholder="Nom du salon" id="roomName" autofocus />
        <div class="input-group-append">
        <button class="btn btn-primary" type="button" id="createRoom">Go</button>
        </div>
        </div>
        </div>
        </div>
        `,
        function () {
            socket.emit('getRooms');
            $('#roomName').focus();
        }
    );
}

function showRooms(rooms) {
    $('#rooms').empty();

    for (let i in rooms) {
        if (!rooms[i].name) return;

        $('#rooms').append(
            `
            <div class="text-center mb-3">
            <span class="room-count">${rooms[i].count}</span>
            <button type="button" class="btn btn-primary room-btn" id="joinRoom">${rooms[i].name}</button>
            </div>
            `
        );
    }
}

function createRoom() {
    let roomName = $('#roomName').val().trim();

    if (!roomName) return;

    socket.emit('createRoom', roomName);
    joinRoom(roomName, true);
}

function joinRoom(roomName, isAdmin = false) {
    changeContent(function () {
        var lt = /</g, 
        gt = />/g, 
        ap = /'/g, 
        ic = /"/g;
        roomNameShow = roomName.toString().replace(lt, "&lt;").replace(gt, "&gt;").replace(ap, "&#39;").replace(ic, "&#34;");

        $('#content').html(
            `
            <h1>${roomNameShow}</h1>
            <h2>Liste des joueurs</h2>
            <ul class="players" id="players">
            </ul>
            <button type="button" class="btn btn-primary" id="leaveRoom">Quitter</button>
            `
        );

        if (isAdmin) {
            addAdminButton();
        }

        socket.emit('joinRoom', roomName);
    });
}

function addAdminButton() {
    $('#content').append('<button type="button" class="btn btn-primary" id="startGame">Lancer la partie</button>');
}

function leaveRoom() {
    socket.emit('leaveRoom');
    goRooms();
}
